#include "param.h"
#include "ui_param.h"

#include <QDebug>

param::param(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::param)
{
    ui->setupUi(this);
    connect(ui->sliderLumi, &QSlider::valueChanged, [=](int val){emit lumiereChanged(val);});
    connect(ui->sliderDist, &QSlider::valueChanged, [=](int val){emit distanceChanged(val*1.0/1000.);});
    connect(ui->sliderRadi, &QSlider::valueChanged, [=](int val){emit radiusChanged(val*1./25000.);});
    connect(ui->sliderNear, &QSlider::valueChanged, [=](int val){
        if(ui->sliderNear->value()>ui->sliderFar->value()){
            int near = ui->sliderNear->value();
            ui->sliderNear->setValue(near-10);
        }setNear(val*1.0/25000.);});
    connect(ui->sliderFar, &QSlider::valueChanged, [=](int val){
        if(ui->sliderNear->value()>ui->sliderFar->value()){
            int far = ui->sliderFar->value();
            ui->sliderFar->setValue(far+1);
        }setFar(val*1.0/25000.);});
    connect(ui->sliderAngl, &QSlider::valueChanged, [=](int val){emit angleChanged(val);});
    connect(ui->sliderRpm, &QSlider::valueChanged, [=](int val){emit rpmChanged(val);});
    connect(ui->checkBox, &QCheckBox::stateChanged, [=](int val){
        if(Qt::Unchecked==val){emit turnedOff();}
        else {emit turnedOn();}    });
}

void param::setDistance(double val){
    if(val != ui->sliderDist->value()){
        ui->sliderDist->setValue((int)(val*1000));
    }
}
void param::setRadius(double val){
    if(val != ui->sliderRadi->value()){
        ui->sliderRadi->setValue((int)(val*25000.));
    }
}
void param::setNear(double val){
    if(val<ui->sliderFar->value() && val != ui->sliderNear->value()){
        ui->sliderNear->setValue((int)(val*25000));
        emit nearChanged(val);
    }
}
void param::setFar(double val){
    if(val>ui->sliderNear->value() && val != ui->sliderFar->value()){
       ui->sliderFar->setValue((int)(val*25000));
       emit farChanged(val);
    }
}
void param::setAngle(double val){
    qDebug()<<"a";
    if(val != ui->sliderAngl->value()){
        qDebug()<<"a";
        ui->sliderAngl->setValue((int)(val));
    }
}
void param::setRpm(int val){
    if(val != ui->sliderRpm->value()){
        ui->sliderRpm->setValue((int)(val));
    }
}
void param::turnOn(){
    ui->checkBox->setCheckState(Qt::Checked);
}
void param::turnOff(){
    ui->checkBox->setCheckState(Qt::Unchecked);
}

param::~param()
{
    delete ui;
}
