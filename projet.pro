#-------------------------------------------------
#
# Project created by QtCreator 2019-01-12T12:27:11
#
#-------------------------------------------------

QT       += core gui quick

CONFIG += c++14

LIBS += -lGLU

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = projet
TEMPLATE = app


SOURCES += main.cpp\
        princ.cpp \
        glarea.cpp \
    cylindre.cpp \
    param.cpp \
    piston.cpp \
    vilbrequin.cpp \
    blocpiston.cpp \
    moteur.cpp \
    culasse.cpp

HEADERS  += princ.h \
        glarea.h \
    cylindre.h \
    param.h \
    piston.h \
    vilbrequin.h \
    blocpiston.h \
    moteur.h \
    culasse.h

FORMS    += princ.ui \
    param.ui

RESOURCES += \
    projet.qrc
