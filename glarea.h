// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef GLAREA_H
#define GLAREA_H

#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QtMath>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

#include "cylindre.h"
#include "piston.h"
#include "blocpiston.h"
#include "moteur.h"

class GLArea : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLArea(QWidget *parent = 0);
    ~GLArea();

private:
    bool isStarted=false;
    int rpm=40;
    double near;
    double far;
    double distance=-1;
    double m_radius = 0.5;
    double m_angle = 0;

    int angle_lumiere=0;

public slots:
    void setDistance(double val);
    void setRadius(double radius);
    void setNear(double val);
    void setFar(double val);
    void setAngle(double val);
    void setLumiere(int val);

    void turnOn();
    void turnOff();
    void setRpm(int rpm);

signals:  // On ne les implémente pas, elles seront générées par MOC ;
          // les paramètres seront passés aux slots connectés.
    void distanceChanged(double value);
    void radiusChanged(double newRadius);
    void nearChanged(double value);
    void farChanged(double value);
    void angleChanged(double value);

    void turnedOn();
    void turnedOff();
    void rpmChanged(int rpm);

protected slots:
    void onTimeout();


protected:
    void initializeGL() override;
    void doProjection();
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;

private:
    void makeGLObjects();
    void tearGLObjects();
    QOpenGLBuffer m_vbo;

private:
    QTimer *m_timer = nullptr;
    double m_anim = 0;
    double m_ratio = 1;

    // Pour utiliser les shaders
    QOpenGLShaderProgram *m_program;
    QOpenGLTexture *m_textures[1]; //Augmenter pour plus de textures !!!!
    int m_posAttr;
    int m_colAttr;
    int m_norAttr;
    int m_matrixUniform;

    double m_alpha;

public:
    QVector<GLfloat> *vertices = new QVector<GLfloat>();
    QVector<GLfloat> *colors = new QVector<GLfloat>();
    int index=0;
    Moteur m = Moteur(vertices, colors, index);
    //Cylindre c = Cylindre(0.2,0.5,4,255,0,0,vertices, colors, index);
};

#endif // GLAREA_H
