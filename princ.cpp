// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "princ.h"
#include <QDebug>

Princ::Princ(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);

    connect(paramBut, &QPushButton::clicked, this, &Princ::openSettings);

    connect(&set,&param::distanceChanged, glarea, &GLArea::setDistance);
    connect(&set,&param::radiusChanged, glarea, &GLArea::setRadius);
    connect(&set,&param::nearChanged, glarea, &GLArea::setNear);
    connect(&set,&param::farChanged, glarea, &GLArea::setFar);
    connect(&set,&param::angleChanged, glarea, &GLArea::setAngle);
    connect(&set,&param::rpmChanged, glarea, &GLArea::setRpm);
    connect(&set,&param::turnedOn, glarea, &GLArea::turnOn);
    connect(&set,&param::turnedOff, glarea, &GLArea::turnOff);


    connect(glarea,&GLArea::distanceChanged, &set, &param::setDistance);
    connect(glarea,&GLArea::radiusChanged, &set, &param::setRadius);
    connect(glarea,&GLArea::angleChanged, &set, &param::setAngle);
    connect(glarea,&GLArea::nearChanged, &set, &param::setNear);
    connect(glarea,&GLArea::farChanged, &set, &param::setFar);

    connect(glarea,&GLArea::turnedOn, &set, &param::turnOn);
    connect(glarea,&GLArea::turnedOff, &set, &param::turnOff);
    connect(glarea,&GLArea::rpmChanged, &set, &param::setRpm);

    connect(&set, &param::lumiereChanged, glarea, &GLArea::setLumiere);
}

void Princ::openSettings(){
    set.show();
}
