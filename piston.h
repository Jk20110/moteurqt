#ifndef PISTON_H
#define PISTON_H

#define pushMatrix(copy, matrix) copy = matrix; m_program->setUniformValue(m_matrixUniform, copy);
#define pullMatrix(copy,matrix) m_program->setUniformValue(m_matrixUniform, matrix);

#include <QVector>
#include <GL/glu.h>
#include <QDebug>
#include <QtMath>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include "cylindre.h"

class Piston
{
public:
    Piston(QVector<GLfloat> *vertices,
           QVector<GLfloat> *colors,
           int &index);
    void drawPiston(QOpenGLShaderProgram *m_program, const QMatrix4x4 &matrix, const int m_matrixUniform, double angle);
    static float bieleLength(){return 0.2f;}
public:
    Cylindre tete, bielle, connecteur;
};

#endif // PISTON_H
