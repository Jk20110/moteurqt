#include "vilbrequin.h"

Vilbrequin::Vilbrequin(QVector<GLfloat> *vertices,
                       QVector<GLfloat> *colors,
                       int &index):
    bloc0(BlocPiston(vertices, colors, index)),
    bloc1(BlocPiston(vertices, colors, index)),
    bloc2(BlocPiston(vertices, colors, index)),
    bloc3(BlocPiston(vertices, colors, index))
{}

void Vilbrequin::draw(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform){
    QMatrix4x4 copy;

    pushMatrix();
    bloc0.draw(m_program, copy, m_matrixUniform);
    pullMatrix();

    pushMatrix();
    copy.translate(bloc0.ep_cycl,0,0);
    copy.rotate(180,1,0,0);
    bloc1.draw(m_program, copy, m_matrixUniform);
    pullMatrix();

    pushMatrix();
    copy.translate(bloc0.ep_cycl+bloc1.ep_cycl,0,0);
    copy.rotate(180,1,0,0);
    bloc2.draw(m_program, copy, m_matrixUniform);
    pullMatrix();

    pushMatrix();
    copy.translate(bloc0.ep_cycl+bloc1.ep_cycl+bloc2.ep_cycl,0,0);
    bloc3.draw(m_program, copy, m_matrixUniform);
    pullMatrix();

}
