#include "blocpiston.h"
#include <QtMath>

inline float squareQuarterRadius(float r){
    double d = qSqrt(2*(r*r))/2;
    return qSqrt((r*r)-(d*d));
}

BlocPiston::BlocPiston(QVector<GLfloat> *vertices,
                       QVector<GLfloat> *colors,
                       int &index):
    disqueG(Cylindre(0.1,0.03,4,52,120,165,vertices,colors,index)),
    disqueD(Cylindre(0.1,0.03,4,156,156,165,vertices,colors,index)),
    maneton(Cylindre(0.05,0.02,8,152,032,165,vertices,colors,index)),
    tourillon(Cylindre(0.05,0.02,8,152,156,48,vertices,colors,index))
{
    float dGr= squareQuarterRadius(disqueG.rayon);
    float dDr= squareQuarterRadius(disqueD.rayon);
    ep_cycl = dGr*2+maneton.ep_cyl+dDr*2+tourillon.ep_cyl;
}

void BlocPiston::draw(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform){
    QMatrix4x4 copy = matrix;
    float dGr= squareQuarterRadius(disqueG.rayon);
    float dDr= squareQuarterRadius(disqueD.rayon);
    pushMatrix();
        copy.rotate(90,0,1,0);
        copy.translate(0, disqueD.ep_cyl-maneton.rayon-tourillon.rayon, dGr+maneton.ep_cyl/2);
        maneton.dessiner_cylindre(m_program, copy, m_matrixUniform);
    pullMatrix();
    pushMatrix();
        copy.translate(0,disqueG.ep_cyl/2-tourillon.rayon,0);
        copy.rotate(90,1,0,0);
        copy.rotate(45,0,0,1);
        disqueG.dessiner_cylindre(m_program, copy, m_matrixUniform);
    pullMatrix();
    pushMatrix();
        copy.translate(dGr+maneton.ep_cyl+dDr,0,0);
        copy.translate(0,disqueD.ep_cyl/2-maneton.rayon,0);
        copy.rotate(90,1,0,0);
        copy.rotate(45,0,0,1);
        disqueD.dessiner_cylindre(m_program, copy, m_matrixUniform);
    pullMatrix();
    pushMatrix();
        copy.rotate(90,0,1,0);
        copy.translate(0, 0, dGr+maneton.ep_cyl+dDr*2+tourillon.ep_cyl/2);
        tourillon.dessiner_cylindre(m_program, copy, m_matrixUniform);
    pullMatrix();

}
