#ifndef BLOCPISTON_H
#define BLOCPISTON_H

#define pushMatrix() {copy = matrix; m_program->setUniformValue(m_matrixUniform, copy);}
#define pullMatrix() { m_program->setUniformValue(m_matrixUniform, matrix);}

#include <QVector>
#include <GL/glu.h>
#include <QDebug>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include "cylindre.h"

class BlocPiston
{
public:
    Cylindre disqueG, disqueD, maneton, tourillon;
    float ep_cycl;
public:
    BlocPiston(QVector<GLfloat> *vertices,
               QVector<GLfloat> *colors,
               int &index);
    void draw(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform);
};

#endif // BLOCPISTON_H
