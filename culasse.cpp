#include "culasse.h"

Culasse::Culasse(double ep_cyl,
                 double rayonI,
                 double rayonE,
                 long nb_facettes,
                 int ouverture,
                 int coul_r, int coul_v,
                 int coul_b,
                 QVector<GLfloat> *vertData,
                 QVector<GLfloat> *colData,
                 int &index)
    :ep_cyl(ep_cyl),
    rayonI(rayonI),
    rayonE(rayonE),
    nb_facettes(nb_facettes),
    coul_r(coul_r),
    coul_v(coul_v),
    coul_b(coul_b),
    debut(index)
{
    ouverture = (ouverture>100)?100:((ouverture<50)?50:ouverture);
    qDebug() << ouverture;
    this->ouverture = ((double)ouverture)/100.0;
    qDebug() << this->ouverture;
    end_tab = generer_forme(vertData, colData);
    index += end_tab[1]+end_tab[2]+end_tab[3]+end_tab[4]+end_tab[5]+end_tab[6];
}

int Culasse::dessiner_cote(QVector<GLfloat> *vertices, QVector<GLfloat> * colData, double z){
    double megaPi = 2 * M_PI;
    int var = 0;
    for (var; var < (int)(nb_facettes*ouverture); var++) {
        vertices->append(rayonI*qCos(var*megaPi/nb_facettes));
        vertices->append(rayonI*qSin(var*megaPi/nb_facettes));
        vertices->append(z);
        colData->append(rayonI*qCos(var*megaPi/nb_facettes));
        colData->append(rayonI*qSin(var*megaPi/nb_facettes));
        colData->append(z);

        appendColors(vertices,0.8);
        colData->append(0.0);
        colData->append(0.0);

        colData->append(1);
        colData->append(1);
        colData->append(1);

        colData->append(1);
        colData->append(1);
        colData->append(1);


        vertices->append(rayonE*qCos(var*megaPi/nb_facettes));
        vertices->append(rayonE*qSin(var*megaPi/nb_facettes));
        vertices->append(z);
        colData->append(rayonE*qCos(var*megaPi/nb_facettes));
        colData->append(rayonE*qSin(var*megaPi/nb_facettes));
        colData->append(z);


        appendColors(vertices,0.8);
        colData->append(1.0);
        colData->append(0.0);

        colData->append(1);
        colData->append(1);
        colData->append(1);

        colData->append(1);
        colData->append(1);
        colData->append(1);


        vertices->append(rayonI*qCos((var+1)*megaPi/nb_facettes));
        vertices->append(rayonI*qSin((var+1)*megaPi/nb_facettes));
        vertices->append(z);
        colData->append(rayonI*qCos((var+1)*megaPi/nb_facettes));
        colData->append(rayonI*qSin((var+1)*megaPi/nb_facettes));
        colData->append(z);

        appendColors(vertices,0.8);
        colData->append(0.0);
        colData->append(1.0);

        colData->append(1);
        colData->append(1);
        colData->append(1);

        colData->append(1);
        colData->append(1);
        colData->append(1);


        vertices->append(rayonE*qCos((var+1)*megaPi/nb_facettes));
        vertices->append(rayonE*qSin((var+1)*megaPi/nb_facettes));
        vertices->append(z);
        colData->append(rayonE*qCos((var+1)*megaPi/nb_facettes));
        colData->append(rayonE*qSin((var+1)*megaPi/nb_facettes));
        colData->append(z);

        appendColors(vertices,0.8);
        colData->append(1.0);
        colData->append(1.0);

        colData->append(1);
        colData->append(1);
        colData->append(1);

        colData->append(1);
        colData->append(1);
        colData->append(1);
    }
    return (var) * 4;
}

int Culasse::dessiner_contour(QVector<GLfloat> *vertices, QVector<GLfloat> * colData, double r){
    double megaPi = 2 * M_PI;
    int var = 0;
    double red=255, ver=100+(ignited)*155., blu=(ignited)*255;

    for (var; var < (int)(nb_facettes*ouverture); var++) {
        QVector3D aU(r*qCos(var*megaPi/nb_facettes),
                     r*qSin(var*megaPi/nb_facettes),
                     +0.5*ep_cyl);
        QVector3D aD(r*qCos(var*megaPi/nb_facettes),
                     r*qSin(var*megaPi/nb_facettes),
                     -0.5*ep_cyl);
        QVector3D nU(r*qCos((var+1)*megaPi/nb_facettes),
                     r*qSin((var+1)*megaPi/nb_facettes),
                     +0.5*ep_cyl);
        QVector3D nD(r*qCos((var+1)*megaPi/nb_facettes),
                     r*qSin((var+1)*megaPi/nb_facettes),
                     -0.5*ep_cyl);

        QVector3D face = -QVector3D::normal(QVector3D(nU-aU),QVector3D(aD-aU));

        colData->append(aU.x());
        colData->append(aU.y());
        colData->append(aU.z());

        colData->append(0.0);
        colData->append(0.0);

        //face = QVector3D::normal(QVector3D(aU-aD),QVector3D(nD-aD));
        colData->append(red/255.);
        colData->append(ver/255.);
        colData->append(blu/255.);

        colData->append(face.x());
        colData->append(face.y());
        colData->append(face.z());


        colData->append(aD.x());
        colData->append(aD.y());
        colData->append(aD.z());

        colData->append(1.0);
        colData->append(0.0);

        colData->append(red/255.);
        colData->append(ver/255.);
        colData->append(blu/255.);

        //face = -QVector3D::normal(QVector3D(aU-nU),QVector3D(nD-nU));
        colData->append(face.x());
        colData->append(face.y());
        colData->append(face.z());


        colData->append(nU.x());
        colData->append(nU.y());
        colData->append(nU.z());

        colData->append(0.0);
        colData->append(1.0);

        colData->append(red/255.);
        colData->append(ver/255.);
        colData->append(blu/255.);

        //face = QVector3D::normal(QVector3D(nU-nD),QVector3D(aD-nD));
        colData->append(face.x());
        colData->append(face.y());
        colData->append(face.z());


        colData->append(nD.x());
        colData->append(nD.y());
        colData->append(nD.z());

        colData->append(1.0);
        colData->append(1.0);

        colData->append(red/255.);
        colData->append(ver/255.);
        colData->append(blu/255.);

        colData->append(face.x());
        colData->append(face.y());
        colData->append(face.z());

        /*vertices->append(r*qCos(var*megaPi/nb_facettes));
        vertices->append(r*qSin(var*megaPi/nb_facettes));
        vertices->append(0.5*ep_cyl);
        colData->append(r*qCos(var*megaPi/nb_facettes));
        colData->append(r*qSin(var*megaPi/nb_facettes));
        colData->append(0.5*ep_cyl);

        appendColors(vertices,0.5);
        colData->append(0.0);
        colData->append(0.0);

        colData->append(1);
        colData->append(1);
        colData->append(1);

        colData->append(1);
        colData->append(1);
        colData->append(1);


        vertices->append(r*qCos(var*megaPi/nb_facettes));
        vertices->append(r*qSin(var*megaPi/nb_facettes));
        vertices->append(-0.5*ep_cyl);
        colData->append(r*qCos(var*megaPi/nb_facettes));
        colData->append(r*qSin(var*megaPi/nb_facettes));
        colData->append(-0.5*ep_cyl);

        appendColors(vertices,0.5);
        colData->append(1.0);
        colData->append(0.0);

        colData->append(1);
        colData->append(1);
        colData->append(1);

        colData->append(1);
        colData->append(1);
        colData->append(1);


        vertices->append(r*qCos((var+1)*megaPi/nb_facettes));
        vertices->append(r*qSin((var+1)*megaPi/nb_facettes));
        vertices->append(0.5*ep_cyl);
        colData->append(r*qCos((var+1)*megaPi/nb_facettes));
        colData->append(r*qSin((var+1)*megaPi/nb_facettes));
        colData->append(0.5*ep_cyl);

        appendColors(vertices,0.5);
        colData->append(0.0);
        colData->append(1.0);

        colData->append(1);
        colData->append(1);
        colData->append(1);

        colData->append(1);
        colData->append(1);
        colData->append(1);


        vertices->append(r*qCos((var+1)*megaPi/nb_facettes));
        vertices->append(r*qSin((var+1)*megaPi/nb_facettes));
        vertices->append(-0.5*ep_cyl);
        colData->append(r*qCos((var+1)*megaPi/nb_facettes));
        colData->append(r*qSin((var+1)*megaPi/nb_facettes));
        colData->append(-0.5*ep_cyl);

        appendColors(vertices,0.5);
        colData->append(1.0);
        colData->append(1.0);

        colData->append(1);
        colData->append(1);
        colData->append(1);

        colData->append(1);
        colData->append(1);
        colData->append(1);*/
    }
    return (var) * 4;
}

int Culasse::dessiner_jointure(QVector<GLfloat> *vertices, QVector<GLfloat> * colData, double angle){
    double megaPi = 2 * M_PI;

    vertices->append(rayonI*qCos(angle));
    vertices->append(rayonI*qSin(angle));
    vertices->append(0.5*ep_cyl);
    colData->append(rayonI*qCos(angle));
    colData->append(rayonI*qSin(angle));
    colData->append(0.5*ep_cyl);

    appendColors(vertices,0.3);
    colData->append(0.0);
    colData->append(0.0);

    colData->append(1);
    colData->append(1);
    colData->append(1);

    colData->append(1);
    colData->append(1);
    colData->append(1);


    vertices->append(rayonE*qCos(angle));
    vertices->append(rayonE*qSin(angle));
    vertices->append(0.5*ep_cyl);
    colData->append(rayonE*qCos(angle));
    colData->append(rayonE*qSin(angle));
    colData->append(0.5*ep_cyl);

    appendColors(vertices,0.3);
    colData->append(1.0);
    colData->append(0.0);

    colData->append(1);
    colData->append(1);
    colData->append(1);

    colData->append(1);
    colData->append(1);
    colData->append(1);


    vertices->append(rayonI*qCos(angle));
    vertices->append(rayonI*qSin(angle));
    vertices->append(-0.5*ep_cyl);
    colData->append(rayonI*qCos(angle));
    colData->append(rayonI*qSin(angle));
    colData->append(-0.5*ep_cyl);

    appendColors(vertices,0.3);
    colData->append(0.0);
    colData->append(1.0);

    colData->append(1);
    colData->append(1);
    colData->append(1);

    colData->append(1);
    colData->append(1);
    colData->append(1);


    vertices->append(rayonE*qCos(angle));
    vertices->append(rayonE*qSin(angle));
    vertices->append(-0.5*ep_cyl);
    colData->append(rayonE*qCos(angle));
    colData->append(rayonE*qSin(angle));
    colData->append(-0.5*ep_cyl);

    appendColors(vertices,0.3);
    colData->append(1.0);
    colData->append(1.0);

    colData->append(1);
    colData->append(1);
    colData->append(1);

    colData->append(1);
    colData->append(1);
    colData->append(1);

    return 4;
}

int * Culasse::generer_forme(QVector<GLfloat> *vertices, QVector<GLfloat> *colData){
    int interieur = dessiner_contour(vertices, colData, rayonI);
    int exterieur = dessiner_contour(vertices, colData,rayonE);
    int rayonHaut = dessiner_cote(vertices, colData, +ep_cyl/2);
    int rayonBas  = dessiner_cote(vertices, colData, -ep_cyl/2);
    int abc = dessiner_jointure(vertices, colData, 0);
    int def = dessiner_jointure(vertices, colData, (2*M_PI*((int)(nb_facettes*ouverture)))/nb_facettes);


    int * tab = new int[7];
    tab[0]=0;
    tab[1]=interieur;
    tab[2]=exterieur;
    tab[3]=rayonHaut;
    tab[4]=rayonBas ;
    tab[5]=abc;
    tab[6]=def;


    return tab;
}

void Culasse::draw(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform){

    QMatrix4x4 copy;
    copy = matrix;
    m_program->setUniformValue(m_matrixUniform, copy);
    glDrawArrays(GL_QUAD_STRIP, debut+end_tab[0], end_tab[1]);
    glDrawArrays(GL_QUAD_STRIP, debut+end_tab[1], end_tab[2]);
    glDrawArrays(GL_QUAD_STRIP, debut+end_tab[1]+end_tab[2], end_tab[3]);
    glDrawArrays(GL_QUAD_STRIP, debut+end_tab[1]+end_tab[2]+end_tab[3], end_tab[4]);
    glDrawArrays(GL_QUAD_STRIP, debut+end_tab[1]+end_tab[2]+end_tab[3]+end_tab[4], end_tab[5]);
    glDrawArrays(GL_QUAD_STRIP, debut+end_tab[1]+end_tab[2]+end_tab[3]+end_tab[4]+end_tab[5], end_tab[6]);
    m_program->setUniformValue(m_matrixUniform, matrix);
}
