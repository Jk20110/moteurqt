#ifndef CULASSE_H
#define CULASSE_H

#include <QVector>
#include <GL/glu.h>
#include <QDebug>
#include <QtMath>
#include <QOpenGLShaderProgram>

class Culasse
{
public:
    double ep_cyl;
    double rayonI,rayonE;
    long nb_facettes;
    int coul_r, coul_v, coul_b;
    int debut;
    int * end_tab;
    double ouverture;
    double ignited=1;

public:
    Culasse(double ep_cyl, double rayonI, double rayonE, long nb_facettes, int ouverture, int coul_r, int coul_v, int coul_b, QVector<GLfloat> *vertData, QVector<GLfloat> *colData, int &index);
    void draw(QOpenGLShaderProgram * m_program, const QMatrix4x4 &matrix, const int m_matrixUniform);
    int dessiner_contour(QVector<GLfloat> *vertData, QVector<GLfloat> *colData, double multiplicateur);
    int * generer_forme(QVector<GLfloat> * vertData, QVector<GLfloat> *colData);
    int dessiner_cote(QVector<GLfloat> *vertices, QVector<GLfloat> *colData, double z);
    int dessiner_jointure(QVector<GLfloat> *vertices, QVector<GLfloat> *colData, double angle);
    void ignite(double angle){
        angle = angle - (M_PI*2)*((int)(angle/(M_PI*2)));

        if(angle > 0 && angle < 5*M_PI/4.){
            //angle = angle-M_PI/2;
            ignited = (angle/(5*M_PI/4.));
        }else{
            ignited = 1;
        }
    }

    inline void appendColors(QVector<GLfloat> * vertices, float mul){
        vertices->append(coul_r / 255. * mul);
        vertices->append(coul_v / 255. * mul);
        vertices->append(coul_b / 255. * mul);
    }
};

#endif // CULASSE_H
